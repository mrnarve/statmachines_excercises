package com.evry.statemachine.statemachine;

import com.evry.statemachine.DataHolders.FileDataHolder;
import com.evry.statemachine.contentparsers.FlatFileContentParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by narve on 03.05.2017.
 */
public class StateParseFlatFile extends ReaderStateEntityBase {
    private static Logger LOGGER = LoggerFactory.getLogger(StateReadFile.class);

    public StateParseFlatFile( ReaderStateMachine parent) {
        super(ReaderStateEnum.PARSE_FLAT_FILE_STATE, parent);
    }

    @Override
    public void entering() {
        LOGGER.info("Entering " + getClass().getSimpleName());

    }

    @Override
    public void handle() throws Exception {
        LOGGER.info("handle " + getClass().getSimpleName());
        FileDataHolder parsedContent = FlatFileContentParser.getParsedContent(parent.getFileContent());
        parent.setFileDataHolder(parsedContent);

        if(parsedContent.isValid()) {
            parent.setState(ReaderStateEnum.VALID_STATE);

        } else {
            parent.setState(ReaderStateEnum.INVALID_STATE);

        }


        parent.handle();
    }

    @Override
    public void exiting() {
        LOGGER.info("exiting " + getClass().getSimpleName());

    }
}
