package com.evry.statemachine.statemachine;

import com.evry.statemachine.DataHolders.FileDataHolder;
import com.evry.statemachine.contentparsers.FlatFileContentParser;
import com.evry.statemachine.contentparsers.XmlContent;
import com.evry.statemachine.contentparsers.XmlContentParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by narve on 03.05.2017.
 */
public class StateParseXmlFile extends ReaderStateEntityBase {
    private static Logger LOGGER = LoggerFactory.getLogger(StateReadFile.class);

    public StateParseXmlFile(ReaderStateMachine parent) {
        super(ReaderStateEnum.PARSE_XML_STATE, parent);
    }

    @Override
    public void entering() {
        LOGGER.info("Entering " + getClass().getSimpleName());

    }

    @Override
    public void handle() throws Exception {
        LOGGER.info("handle " + getClass().getSimpleName());
        XmlContent parsedContent = XmlContentParser.getXmlContentFromString(parent.getFileContent());
        parent.setFileDataHolder(XmlContentParser.getFileDataFromXmlContent(parsedContent));
        if(parent.getFileDataHolder().isValid()) {
            parent.setState(ReaderStateEnum.VALID_STATE);

        } else {
            parent.setState(ReaderStateEnum.INVALID_STATE);

        }


        parent.handle();
    }

    @Override
    public void exiting() {
        LOGGER.info("exiting " + getClass().getSimpleName());

    }
}
