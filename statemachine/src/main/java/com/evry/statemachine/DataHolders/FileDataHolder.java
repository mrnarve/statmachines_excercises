package com.evry.statemachine.DataHolders;

import com.evry.statemachine.statemachine.ReaderStateEnum;

import java.time.LocalDate;

public class FileDataHolder {
    private String company;
    private String startedBy;
    private LocalDate startedDate;

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getStartedBy() {
        return startedBy;
    }

    public void setStartedBy(String startedBy) {
        this.startedBy = startedBy;
    }

    public LocalDate getStartedDate() {
        return startedDate;
    }

    public void setStartedDate(LocalDate startedDate) {
        this.startedDate = startedDate;
    }

    public static Builder with() {
        return new Builder();
    }

    public static class Builder {
        protected FileDataHolder fileDataHolder;

        public Builder() {
            fileDataHolder = new FileDataHolder();
        }

        public Builder company(String company) {
            fileDataHolder.setCompany(company);
            return this;
        }

        public Builder startedBy(String startedBy) {
            fileDataHolder.setStartedBy(startedBy);
            return this;
        }

        public Builder startedDate(LocalDate startedDate) {
            fileDataHolder.setStartedDate(startedDate);
            return this;
        }

        public FileDataHolder build() {
            return fileDataHolder;
        }

    }

    public boolean isValid() {
        return !(this.getStartedBy().contains("@") || this.getCompany().contains("@"));
    }
}
