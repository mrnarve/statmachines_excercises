package com.evry.statemachine.statemachine;

import org.junit.Assert;
import org.junit.Test;

public class TestStatemachineBasics {

    private String testXml = "<?xml version=\"1.1\" encoding=\"UTF-8\" ?>\n" +
            "<com:content xmlns:com=\"com.evry.statemachine.xml\">\n" +
            "  <company>someCompany</company>\n" +
            "  <startedBy>anonymous</startedBy>\n" +
            "  <startedDate>2008-09-29</startedDate>\n" +
            "</com:content>\n";

    private String flatFileContents = "someCompany|anonymous|2008-09-29|\n";

    @Test
    public void testStateMachineFaultHandling() throws Exception {
        ReaderStateMachine readerStateMachine = new ReaderStateMachine();
        readerStateMachine.handle();

        Assert.assertEquals(ReaderStateEnum.FAULT_STATE, readerStateMachine.getCurrentState());
    }

    @Test
    public void testStateMachineReadFlatFile() throws Exception {
        ReaderStateMachine readerStateMachine = new ReaderStateMachine();
        readerStateMachine.setFilePath("test.txt");
        readerStateMachine.handle();

        Assert.assertEquals(flatFileContents, readerStateMachine.getFileContent());
    }

    @Test
    public void testStateMachineReadXml() throws Exception {
        ReaderStateMachine readerStateMachine = new ReaderStateMachine();
        readerStateMachine.setFilePath("test.xml");
        readerStateMachine.handle();

        Assert.assertEquals(testXml, readerStateMachine.getFileContent());
    }
}
