package com.evry.statemachine.statemachine;

import com.evry.statemachine.DataHolders.FileDataHolder;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class TestExcercise1 {

    String xmlFile = "test.xml";
    String flatFile = "test.txt";

    @Test
    public void testParseXmlFile() throws Exception {
        ReaderStateMachine readerStateMachine = new ReaderStateMachine();
        readerStateMachine.setFilePath("test.xml");
        readerStateMachine.handle();

        FileDataHolder fileDataHolder = readerStateMachine.getFileDataHolder();
        Assert.assertNotNull("filedataholder is not set", fileDataHolder);

        Assert.assertEquals("someCompany", fileDataHolder.getCompany());
        Assert.assertEquals("anonymous", fileDataHolder.getStartedBy());
        Assert.assertEquals("2008-09-29", fileDataHolder.getStartedDate().toString());
    }

    @Test
    public void testParseFlatFile() throws Exception {
        ReaderStateMachine readerStateMachine = new ReaderStateMachine();
        readerStateMachine.setFilePath("test.txt");
        readerStateMachine.handle();

        FileDataHolder fileDataHolder = readerStateMachine.getFileDataHolder();

        Assert.assertNotNull("filedataholder is not set", fileDataHolder);
        Assert.assertEquals("someCompany", fileDataHolder.getCompany());
        Assert.assertEquals("anonymous", fileDataHolder.getStartedBy());
        Assert.assertEquals("2008-09-29", fileDataHolder.getStartedDate().toString());
    }
}
