package com.evry.statemachine.statemachine;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class TestExcercise2 {

    String xmlFile = "test.xml";
    String flatFile = "test.txt";

    @Test
    public void testParseXmlFile() throws Exception {
        ReaderStateMachine readerStateMachine = new ReaderStateMachine();
        readerStateMachine.setFilePath("test.xml");
        readerStateMachine.handle();

        Assert.assertEquals(ReaderStateEnum.VALID_STATE, readerStateMachine.getCurrentState());


    }

    @Test
    public void testParseFlatFile() throws Exception {
        ReaderStateMachine readerStateMachine = new ReaderStateMachine();
        readerStateMachine.setFilePath("test.txt");
        readerStateMachine.handle();

        Assert.assertEquals(ReaderStateEnum.VALID_STATE, readerStateMachine.getCurrentState());
    }

    @Test
    public void testParseFlatFileInvalidContent() throws Exception {
        ReaderStateMachine readerStateMachine = new ReaderStateMachine();
        readerStateMachine.setFilePath("testInvalid.txt");
        readerStateMachine.handle();

        Assert.assertEquals(ReaderStateEnum.INVALID_STATE, readerStateMachine.getCurrentState());
    }

    @Test
    public void testParseXmlFileInvalidContent() throws Exception {
        ReaderStateMachine readerStateMachine = new ReaderStateMachine();
        readerStateMachine.setFilePath("testInvalid.xml");
        readerStateMachine.handle();

        Assert.assertEquals(ReaderStateEnum.INVALID_STATE, readerStateMachine.getCurrentState());
    }
}
