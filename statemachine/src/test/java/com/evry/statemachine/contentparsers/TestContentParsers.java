package com.evry.statemachine.contentparsers;

import com.evry.statemachine.DataHolders.FileDataHolder;
import org.junit.Assert;
import org.junit.Test;

import javax.xml.bind.UnmarshalException;

public class TestContentParsers {

    private String testXml = "<com:content xmlns:com=\"com.evry.statemachine.xml\">\n" +
            "  <company>someCompany</company>\n" +
            "  <startedBy>anonymous</startedBy>\n" +
            "  <startedDate>2008-09-29</startedDate>\n" +
            "</com:content>";

    private String invalidXml = "<com:content xmlns:com=\"com.evry.statemachine.xml\">\n" +
            "  <company>someCompany</company>\n" +
            "  <startedBy>anonymous</startedBy>\n" +
            "  <startedBy>anonymous</startedBy>\n" +
            "  <startedBy>anonymous</startedBy>\n" +
            "  <startedDate>2008-09-29</startedDate>\n" +
            "</com:content>";

    private String flatFileContents = "someCompany|anonymous|2008-09-29|";

    @Test
    public void xmlParseTest() throws Exception {
        XmlContent content = XmlContentParser.getXmlContentFromString(testXml);

        Assert.assertEquals("someCompany", content.getCompany());
        Assert.assertEquals("anonymous", content.getStartedBy());
        Assert.assertEquals("2008-09-29", content.getStartedDate().toString());
    }

    @Test
    public void xmlParseToDataHolderTest() throws Exception {
        XmlContent content = XmlContentParser.getXmlContentFromString(testXml);

        FileDataHolder fileDataHolder = XmlContentParser.getFileDataFromXmlContent(content);

        Assert.assertEquals("someCompany", fileDataHolder.getCompany());
        Assert.assertEquals("anonymous", fileDataHolder.getStartedBy());
        Assert.assertEquals("2008-09-29", fileDataHolder.getStartedDate().toString());
    }

    @Test(expected=UnmarshalException.class)
    public void xmlValidationTest() throws Exception {
        XmlContent content = XmlContentParser.getXmlContentFromString(invalidXml);
    }

    @Test
    public void testFlatFileContentParser() throws Exception {
        FileDataHolder fileDataHolder = FlatFileContentParser.getParsedContent(flatFileContents);

        Assert.assertEquals("someCompany", fileDataHolder.getCompany());
        Assert.assertEquals("anonymous", fileDataHolder.getStartedBy());
        Assert.assertEquals("2008-09-29", fileDataHolder.getStartedDate().toString());
    }
}
