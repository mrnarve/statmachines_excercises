package com.evry.statemachine.common;

public abstract class StateEntityBase<E extends Enum, P extends StateMachineBase> {

    private final E stateEnum;
    protected final P parent;

    public StateEntityBase(E stateEnum, P parent) {
        this.stateEnum = stateEnum;
        this.parent = parent;
    }

    public abstract void entering();
    public abstract void handle() throws Exception;
    public abstract void exiting();

    public E getStateEnum() {return stateEnum;}
}
